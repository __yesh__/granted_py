#What We Take For Granted

##To run it locally:

1. Clone the file
2. cd into the granted_py directory cd/granted_py
3. Open index.html

##To be able to access the notes on the slides:

1. Install [Node.js](http://nodejs.org/)
2. Install [Grunt](http://gruntjs.com/getting-started#installing-the-cli)
3. grunt serve

For more details check out [Reveal.js](https://github.com/hakimel/reveal.js/)
